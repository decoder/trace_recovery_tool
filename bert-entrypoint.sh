#!/bin/bash

set -o errexit
set -o pipefail
set -o nounset

echo "bert-serving-start -model_dir /BERT -num_worker=4 -cpu -port ${BERT_PORT} -port_out ${BERT_PORT_OUT} -http_port ${BERT_HTTP_PORT}"
bert-serving-start -model_dir /BERT -num_worker=4 -cpu -port ${BERT_PORT} -port_out ${BERT_PORT_OUT} -http_port ${BERT_HTTP_PORT}
