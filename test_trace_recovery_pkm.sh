#!/bin/bash

set -o errexit
set -o pipefail
set -o nounset

DIR="${BASH_SOURCE%/*}"
if [[ ! -d "$DIR" ]]; then DIR="$PWD"; fi
. "$DIR/pkm_rest_api.sh"

PKM_USER=garfield
PKM_PASSWORD=hello
PROJECT_NAME="mydb"

host="localhost"
port=703


function encode
{
  result=$(python -c "import urllib.parse;print(urllib.parse.quote('$1'))")
  echo $result
}

PKM_PATH_SRC=$(encode "code/c/comments/mydb" )
PKM_PATH_TGT=$(encode "code/c/comments/mydb" )
# [{"global_kind":"GCompTag","loc":{"pos_start":{"pos_path":"examples/vector2.c","pos_lnum":5,"pos_bol":107190,"pos_cnum":107190},"pos_end":{"pos_path":"examples/vector2.c","pos_lnum":5,"pos_bol":107190,"pos_cnum":107196}},"comments":["#include \"stdio.h\"\\n"]},{"global_kind":"GFun","loc":{"pos_start":{"pos_path":"examples/vector2.c","pos_lnum":20,"pos_bol":107483,"pos_cnum":107488},"pos_end":{"pos_path":"examples/vector2.c","pos_lnum":25,"pos_bol":107640,"pos_cnum":107641}},"comments":[" Sum of two vectors\\n"]},{"global_kind":"GFun","loc":{"pos_start":{"pos_path":"examples/vector2.c","pos_lnum":43,"pos_bol":108059,"pos_cnum":108063},"pos_end":{"pos_path":"examples/vector2.c","pos_lnum":46,"pos_bol":108182,"pos_cnum":108183}},"comments":[" Scalar product of two vectors\\n"]},{"stmt_id":27,"loc":{"pos_start":{"pos_path":"examples/vector2.c","pos_lnum":70,"pos_bol":108604,"pos_cnum":108605},"pos_end":{"pos_path":"examples/vector2.c","pos_lnum":70,"pos_bol":108604,"pos_cnum":108614}},"comments":["\tfprintf(stdout,\"%f\\n\",norm(c));\\n"]}]

# JSonpath syntax
# $[*]["comments"][*]
# $[?(global_kind == "GFun")].comments

# jsonpath_rw_ext (does not work)
# ACCESS_SRC=$(encode '$[?(global_kind = "GFun" & loc.pos_start.pos_path = "examples/vector2.c" )].comments[0]')
# ACCESS_TGT=$(encode '$[?(global_kind = "GFun" & loc.pos_start.pos_path = "examples/vector2.c" )].comments[1]')

# jg syntax https://stedolan.github.io/jq/manual
# ACCESS_SRC=$(encode '[.[] | select(.global_kind=="GFun") | select(.loc.pos_start.pos_path=="examples/vector2.c") | .comments][0]')
# ACCESS_TGT=$(encode '[.[] | select(.global_kind=="GFun") | select(.loc.pos_start.pos_path=="examples/vector2.c") | .comments][1]')

# JMESPath syntax
ACCESS_SRC=$(encode "[?global_kind == \'GFun\'] | [?loc.pos_start.pos_path == \'examples/vector2.c\'].comments | [0]")
ACCESS_TGT=$(encode "[?global_kind == \'GFun\'] | [?loc.pos_start.pos_path == \'examples/vector2.c\'].comments | [1]")

USER1_KEY=$(login ${PKM_USER} ${PKM_PASSWORD})
echo "Login was successful"

command="curl --request POST  --url http://${host}:${port}/pkm_trace_recovery --header 'content-type: application/json' --header 'accept: application/json' --header 'key: ${USER1_KEY}' --data '{\"project_id\": \"${PROJECT_NAME}\", \"src_path\": \"${PKM_PATH_SRC}\", \"src_access\": \"${ACCESS_SRC}\", \"tgt_path\": \"${PKM_PATH_TGT}\", \"tgt_access\": \"${ACCESS_TGT}\"}'"

cat <<EOF
command:
$command

response:
$(eval ${command})
EOF
