#!/usr/bin/env python3

import numpy as np
import os
import sys
from bert_serving.client import BertClient


def scoring(pair, bc):
    import math
    query_vec_1, query_vec_2 = bc.encode(pair)
    cosine = (np.dot(query_vec_1, query_vec_2)
              / (np.linalg.norm(query_vec_1) * np.linalg.norm(query_vec_2)))
    return 1/(1 + math.exp(-100*(cosine - 0.95)))


class TraceRecovery(object):

    @staticmethod
    def trace_recovery(text1: str, text2: str):
        '''Extract traceability links between texts
        '''
        host = os.environ.get("BERT_SERVER")
        port = int(os.environ.get("BERT_PORT"))
        port_out = int(os.environ.get("BERT_PORT_OUT"))

        print(f"TraceRecovery.trace_recovery {host}, {port}, {port_out}",
              file=sys.stderr)

        with BertClient(ip=host,
                        port=port,
                        port_out=port_out,
                        check_version=False) as bc:
            score = scoring([text1, text2], bc)
            print(f"Score is {score}", file=sys.stderr)
        result = {
            "similarity": score,
            "role": "",
            "artifactId": "",
            }

        return True, result
