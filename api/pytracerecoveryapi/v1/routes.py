#!/usr/bin/env python3

import jmespath
import json
import logging
import os
import sys
import time
import urllib.parse

import flask
from flask import (Blueprint, request, jsonify, make_response, abort)
from marshmallow import Schema, fields, validate

from pkm_python_client.pkm_client import PKMClient
from pkm_python_client.pkm_client import Log

from .decoder_trace_recovery_tool_service import TraceRecovery

PKM_USER = os.environ.get('PKM_USER')
PKM_PASSWORD = os.environ.get('PKM_PASSWORD')
PKM_CERT = os.environ.get('PKM_CERT')
if PKM_CERT is None:
    PKM_CERT = "/ssl/pkm_docker.crt"


debug = os.environ.get('DEBUG', default="true").lower() == "true"

api = Blueprint('api_v1', __name__)

class PKMTraceRecoverySchema(Schema):
    project_id = fields.Str(required=True)  # pkm project to query
    # pkm path to query to retrieve json data containing the source text
    src_path = fields.Str(required=True)
    # json path allowing to retrieve the source text to analyze in the
    # retrieved data
    src_access = fields.Str(required=False)
    # pkm path to query to retrieve json data containing the target text
    tgt_path = fields.Str(required=True)
    # json path allowing to retrieve the target text to analyze in the
    # retrieved data
    tgt_access = fields.Str(required=False)
    # the trace recovery model to use. Currently unused.
    model = fields.Str(required=False)
    invocationID = fields.Str(required=False)


class TraceRecoveryResult:
    """
    {
        "similarity": "float value",
        "role": "string",
        "artifactId": "string",
    }

    """
    def __init__(self, similarity, role, artifactId):
        self.similarity = similarity
        self.role = role
        self.artifactId = artifactId


class TraceRecoveryResultSchema(Schema):
    """
    {
        "similarity": "float value",
        "role": "string",
        "artifactId": "string",
    }

    """

    similarity = fields.Float(required=True)
    role = fields.Str(required=True)
    artifactId = fields.Str(required=True)


class PKMTraceRecoveryResult:
    def __init__(self, project_id,
                 src_path, src_access, src_text,
                 tgt_path, tgt_access, tgt_text,
                 similarity, role, model, invocationID):
        self.project_id = project_id
        self.src_path = src_path
        self.src_access = src_access
        self.src_text = src_text
        self.tgt_path = tgt_path
        self.tgt_access = tgt_access
        self.tgt_text = tgt_text
        self.similarity = similarity
        self.role = role
        self.model = model
        self.invocationID = invocationID


class PKMTraceRecoveryResultSchema(Schema):
    project_id = fields.Str(required=True)  # pkm project to query
    # pkm path to query to retrieve json data containing the source text
    src_path = fields.Str(required=True)
    # json path allowing to retrieve the source text to analyze in the
    # retrieved data
    src_access = fields.Str(required=False)
    # The source text
    src_text = fields.Str(required=True)
    # pkm path to query to retrieve json data containing the target text
    tgt_path = fields.Str(required=True)
    # json path allowing to retrieve the target text to analyze in the
    # retrieved data
    tgt_access = fields.Str(required=False)
    # The target text
    tgt_text = fields.Str(required=True)
    # the trace recovery model to use. Currently unused.
    similarity = fields.Float(required=True)
    role = fields.Str(required=True)
    model = fields.Str(required=False)
    invocationID = fields.Str(required=False)


class PKMTraceRecoveryErrorSchema (Schema):
    message = fields.Str(required=True)
    status = fields.Boolean(required=True)


class Texts(Schema):
    text1 = fields.Str(required=True)
    text2 = fields.Str(required=True)


def send_result(result: str, status: int, pkm: PKMClient = None,
                project_id: str = None, invocation_id: str = None,
                resultsToAdd=[]):
    logging.info(f"send_result message: {result}, status: {status}, "
                 f"project_id: {project_id}, invocation_id: {invocation_id}")
    assert type(resultsToAdd) == list, (f"resultsToAdd ({resultsToAdd}) should be a "
                                        f"list but it is a {type(resultsToAdd)}")
    app = flask.current_app
    if status < 300:
        schema = TraceRecoveryResultSchema()
        loaded = schema.loads(result)
        result_string = schema.dumps(loaded)
    else:
        res = {
            "message": result,
            "status": (status < 300)}
        app.logger.error(result)
        error_result_schema = PKMTraceRecoveryErrorSchema()
        loaded = error_result_schema.load(res)
        result_string = error_result_schema.dumps(loaded)

    logging.info(f"Returning {status}, {result_string}.")
    if invocation_id is not None:
        # Put in invocationResults the paths of artifacts put in the pkm with type of
        # handled data ?
        pkm.update_invocation(
            project_id, invocation_id, 0 if status < 300 else status,
            resultsToAdd)
    return result_string, status


@api.route('/pkm_trace_recovery', methods=['POST'])
def pkm_trace_recovery(invocationID=None):
    """ Trace recovery from text stored in the PKM
    Write result in the traceability matrix collection of the PKM.
    Data below --- is defining the API. It is used by stuff to instancy and
    check data
    ---
    post:
      parameters:
      - in: header
        name: key
        description: Access key to the PKM (optional, depending on server
                      configuration)
        schema:
          type: string
      - name: invocationID
        in: path
        description: decoder invocation id
        required: false
        schema:
          type: string
      - in: data
        schema: PKMTraceRecoverySchema
      responses:
        200:
          description: Trace recovery completed.
          content:
            application/json:
              schema: TraceRecoveryResultSchema
        400:
          description: Bad Request.
          content:
            application/json:
              schema: PKMTraceRecoveryErrorSchema
        500:
          description: An unexpected error has occurred.
          content:
            application/json:
              schema: PKMTraceRecoveryErrorSchema
    """
    try:
        app = flask.current_app

        key = None
        if 'key' in request.headers:
            key = request.headers.get('key')
        elif 'keyParam' in request.headers:
            key = request.headers.get('keyParam')

        form_data = request.get_json()
        project_id = None
        if "project_id" in form_data:
            project_id = form_data['project_id']
        invocation_id = request.args.get('invocationID', None)
        if invocation_id is None and 'invocationID' in form_data:
                invocation_id = form_data['invocationID']
        print(f"pkm_trace_recovery project_id: {project_id}, invocation_id: {invocation_id}",
            file=sys.stderr)

        pkm = PKMClient(cacert=PKM_CERT, key=key)
        if key is None and PKM_USER and PKM_PASSWORD:
            if debug:
                flask.current_app.logger.debug("user's login")
            status = pkm.login(user_name=PKM_USER, user_password=PKM_PASSWORD)
            if not status:
                message = f"user's login to pkm failed: {status}"
                flask.current_app.logger.error(message)
                return send_result(message, 500, pkm, project_id, invocation_id)
        elif key is None:
            errorString = (
            "Either pkm key should be passed as http header 'key' or"
            " user/password env vars should be defined")
            flask.current_app.logger.error(errorString)
            return send_result(errorString, 400, pkm, project_id, invocation_id)

        log = Log(pkm=pkm, project=project_id, tool="trace_recovery_tool", invocation_id=invocation_id)

        trace_recovery_schema = PKMTraceRecoverySchema()
        errors = trace_recovery_schema.validate(form_data)
        if errors:
            message = (f"/pkm_trace_recovery Failed to validate request "
                    f"data '{form_data}' with respect to the schema. "
                    f"Error: {errors}.")
            log.error(message)
            app.logger.error(message)
            return send_result(message, 400, pkm, project_id, invocation_id)

        project_id = form_data['project_id']  # pkm project to query
        # pkm path to query to retrieve json data containing the text to analyze
        src_path = urllib.parse.unquote(form_data['src_path'])
        # pkm path to query to retrieve json data containing the text to analyze
        tgt_path = urllib.parse.unquote(form_data['tgt_path'])
        # access: json path allowing to retrieve the text to analyze in the
        # retrieved data
        if 'src_access' in form_data:
            src_access = form_data['src_access']
        else:
            src_access = None
        if 'tgt_access' in form_data:
            tgt_access = form_data['tgt_access']
        else:
            tgt_access = None

        # model, currently unused, will be used to select models specialy
        # trained for a given use case
        if 'model' in form_data:
            model = form_data['model']
        else:
            model = None

        if debug:
            message = f"getting {src_path}"
            app.logger.debug(message)
            log.message(message)
        status, src_doc = pkm.call(method="GET", path=src_path)
        if status != 0:
            message = f"Getting {src_path} failed."
            app.logger.error(message)
            log.error(message)
            return send_result(message, 500, pkm, project_id, invocation_id)
        if debug:
            message = f"getting {tgt_path}"
            app.logger.debug(message)
            log.message(message)
        status, tgt_doc = pkm.call(method="GET", path=tgt_path)
        if status != 0:
            message = f"Getting {tgt_path} failed."
            app.logger.error(message)
            log.error(message)
            return send_result(message, 500, pkm, project_id, invocation_id)

        if debug:
            message = f"retrieved src_doc is {src_doc}"
            app.logger.debug(message)
            log.message(message)
            message = f"retrieved tgt_doc is {tgt_doc}"
            app.logger.debug(message)
            log.message(message)
        if src_access is None:
            src_text = src_doc
            if not isinstance(src_text, str):
                message = f"Source is not a single text."
                app.logger.error(message)
                log.error(message)
                return send_result(message, 500, pkm, project_id, invocation_id)
        else:
            if debug:
                message = (
                f"source jmes json path is: {urllib.parse.unquote(src_access)}")
                app.logger.debug(message)
                log.message(message)
            try:
                match = jmespath.search(urllib.parse.unquote(src_access),
                                        src_doc)
            except jmespath.exceptions.EmptyExpressionError as e:
                message = f"JMESpath search failed on empty src access {src_access}: {e}"
                app.logger.debug(message)
                log.message(message)
                return send_result(message, 400, pkm, project_id, invocation_id)
            except Exception as e:
                message = f"JMESpath search failed on src access {src_access}: {e}"
                app.logger.debug(message)
                log.message(message)
                return send_result(message, 400, pkm, project_id, invocation_id)
            if match is None:
                message = (f"Source jmes json path match "
                        f"{urllib.parse.unquote(src_access)} did not match "
                        f"anything")
                app.logger.error(message)
                log.error(message)
                return send_result(message, 500, pkm, project_id, invocation_id)
            elif debug:
                message = f"source match is: {match}"
                app.logger.debug(message)
                log.message(message)
            # jsonpath_expression = jsonpath_rw_ext.parse(
            # urllib.parse.unquote(src_access))

            # match = jsonpath_expression.find(src_doc)
            # src_text = [text.value for text in match][0]
            src_text = ''.join(match)
            if debug:
                message = f"selected source text is: {src_text}"
                app.logger.debug(message)
                log.message(message)
        if tgt_access is None:
            tgt_text = tgt_doc
            if not isinstance(tgt_text, str):
                message = f"Target is not a single text."
                app.logger.error(message)
                log.error(message)
                return send_result(message, 500, pkm, project_id, invocation_id)
        else:
            if debug:
                message = (
                f"target json path is: {urllib.parse.unquote(tgt_access)}")
                app.logger.debug(message)
                log.message(message)
            try:
                match = jmespath.search(urllib.parse.unquote(tgt_access),
                                        tgt_doc)
            except jmespath.exceptions.EmptyExpressionError as e:
                message = f"JMESpath search failed on empty tgt access {tgt_access}: {e}"
                app.logger.debug(message)
                log.message(message)
                return send_result(message, 400, pkm, project_id, invocation_id)
            except Exception as e:
                message = f"JMESpath search failed on tgt access {tgt_access}: {e}"
                app.logger.debug(message)
                log.message(message)
                return send_result(message, 400, pkm, project_id, invocation_id)
            if match is None:
                message = (f"Target jmes json path match "
                        f"{urllib.parse.unquote(tgt_access)} did not match "
                        f"anything")
                app.logger.error(message)
                log.error(message)
                return send_result(message, 500, pkm, project_id, invocation_id)
            elif debug:
                message = f"target match is: {match}"
                app.logger.debug(message)
                log.message(message)
            # jsonpath_expression = jsonpath_rw_ext.parse(
            # urllib.parse.unquote(tgt_access))

            # match = jsonpath_expression.find(tgt_doc)
            # tgt_text = [text.value for text in match][0]
            tgt_text = ''.join(match)
            if debug:
                message = f"selected target text is: {tgt_text}"
                app.logger.debug(message)
                log.message(message)

        results = []
        message = f"pkm_trace_recovery {src_text} and {tgt_text}."
        logging.info(message)
        log.message(message)
        status, res = TraceRecovery.trace_recovery(src_text, tgt_text)

        if status is True:
            message = f"Trace recovery completed {status}, {res}."
            logging.info(message)
            log.message(message)
        else:
            message = (
            f"Internal error while computing trace of project {project_id} "
            f"and paths {src_path} and {tgt_path}")
            app.logger.error(message)
            log.error(message)
            return send_result(message, 500, pkm, project_id, invocation_id)

        result = PKMTraceRecoveryResult(
            project_id = project_id,
            src_path=src_path,
            src_access=src_access,
            src_text=src_text,
            tgt_path=tgt_path,
            tgt_access=tgt_access,
            tgt_text=tgt_text,
            model=model,
            invocationID=invocation_id,
            similarity=res["similarity"],
            role=res["role"],
            )

        schema = PKMTraceRecoveryResultSchema()
        dumped_result = schema.dump(result)

        # TODO implement writing the srl at the right location in the annotations.
        path = f"traceability_matrix/{project_id}"
        if debug:
            message = (f"writing back trace_recovery result of project {project_id} "
                    f"and path {urllib.parse.quote(path, safe='')}")
            app.logger.debug(message)
            log.message(message)
            message = f"result to write is: {schema.dumps(result)}"
            app.logger.debug(message)
            log.message(message)
        # pkm response schema for pkm_res
        # this is expected to be an array of 1 artifactId
        # application/json:
        # schema:
        # description: IDs of inserted artefacts (i.e. traceability matrix rows)
        # type: array
        # items:
        # description: ID of inserted artefact (i.e. traceability matrix row)
        # type: string
        status, pkm_res = pkm.call(method="POST", path=path,
                                payload=[dumped_result])
        if status != 0:
            message = (
            f"Writing back trace recovery result of project {project_id} "
            f"and paths {src_path} and {tgt_path} failed")
            app.logger.error(message)
            log.error(message)
            return send_result(message, 500, pkm, project_id, invocation_id)

        result_to_send = TraceRecoveryResult(
            similarity=res["similarity"],
            role=res["role"],
            artifactId=pkm_res[0])
        result_schema = TraceRecoveryResultSchema()

        if debug:
            message = f"Returning {result_schema.dumps(result_to_send)}."
            logging.info(message)
            log.message(message)

        return send_result(result_schema.dumps(result_to_send), 200, pkm, project_id,
                        invocation_id,
                        [dict(path=(
                            f"/annotations/{project_id}"
                            f"?src_path={urllib.parse.quote(src_path, safe='')}"
                            f"&src_access={urllib.parse.quote(src_access, safe='')}"
                            f"?tgt_path={urllib.parse.quote(tgt_path, safe='')}"
                            f"&tgt_access={urllib.parse.quote(tgt_access, safe='')}"),
                            type="annotations")])
    except Exception as e:
        message = (f"Got an unexpected exception in pkm_trace_recovery: {e}")
        app.logger.error(message)
        log.error(message)
        return send_result(message, 500, pkm=None, project_id=None, invocation_id=None)


@api.route('/trace_recovery', methods=['POST'])
def trace_recovery():
    """Do the trace recovery
    Data below --- is defining the API. It is used by stuff to instancy and
    check data
    ---
    post:
      parameters:
      - in: data
        schema: Texts
      responses:
        200:
          schema: TraceRecoveryResult
    """

    app = flask.current_app
    text_schema = Texts()
    form_data = request.get_json()
    # This below validates the JSON data existence and data type
    errors = text_schema.validate(form_data)
    if errors:
        return {
          "message": "Missing or sending incorrect data to analyze text."
            }, 400
    else:

        text1 = form_data['text1']
        text2 = form_data['text2']
        # if text != "whichptr indicates which xbuffer holds the final
        # iMCU row.":
        # abort(501)

        logging.info(f"Tracing {text1} and {text2}.")
        status, res = TraceRecovery.trace_recovery(text1, text2)

        if status is True:
            logging.info(f"Analysis completed {status}, {res}.")

        result_schema = TraceRecoveryResult()
        loaded = result_schema.load(res)

        logging.info(f"Returning {result_schema.dumps(loaded)}.")
        return result_schema.dumps(loaded), 200


