#!/usr/bin/env python3

import os
from logging.config import dictConfig
import json
from flask import Flask
from apispec import APISpec
from apispec.ext.marshmallow import MarshmallowPlugin
from apispec_webframeworks.flask import FlaskPlugin
from marshmallow import Schema, fields
import ca_bundle
from flask import Blueprint
from flask import jsonify


from .v1.routes import api as api_v1
from .v1.routes import trace_recovery as trace_recovery
from .v1.routes import pkm_trace_recovery as pkm_trace_recovery

ca_bundle.install()

spec = APISpec(
    title="Decoder Trace Recovery tool",
    version="0.0.2",
    openapi_version="3.0.2",
    info=dict(description="This is the Decoder project tool dedicated to the discovery of traceability links between artifacts"),
    plugins=[FlaskPlugin(), MarshmallowPlugin()],
)


#def _initialize_errorhandlers(application):
    #'''
    #Initialize error handlers
    #'''
    #import errors
    #application.register_blueprint(errors)


bp = Blueprint('home', __name__)


@bp.route('/')
def home():
    return jsonify(spec.to_dict())


def create_app():

    dictConfig({
        'version': 1,
        'formatters': {'default': {
            'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s',
        }},
        'handlers': {'wsgi': {
            'class': 'logging.StreamHandler',
            'stream': 'ext://flask.logging.wsgi_errors_stream',
            'formatter': 'default'
        }},
        'root': {
            'level': 'DEBUG',
            'handlers': ['wsgi']
        }
    })

    # Create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config['SECRET_KEY'] = 'b0bff1b3-32d1-4ec5-b6e6-f31af9ed682e'

    with app.app_context():

        # Register home blueprint
        app.register_blueprint(bp)
        #_initialize_errorhandlers(app)

        # Register api v1
        app.register_blueprint(api_v1, url_prefix="/")

        return app


app = create_app()

# we need to be in a Flask request context
with app.test_request_context():
    spec.path(view=trace_recovery)
    spec.path(view=pkm_trace_recovery)

if __name__ == "__main__":
    app.run(debug=True)

