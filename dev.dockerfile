FROM tiangolo/uwsgi-nginx-flask:python3.8

# --------------------
# Package installation
# --------------------

ARG DEBIAN_FRONTEND=noninteractive
ARG THREADS
ARG PUID=2030
ARG GUID=2030

ENV PUID ${PUID}
ENV GUID ${GUID}

# CREATE APP USER
RUN groupadd -g ${GUID} apiuser && \
useradd -r -u ${PUID} -g apiuser apiuser

COPY requirements.txt /
RUN pip install --upgrade pip
RUN pip install -r /requirements.txt

COPY ./api /app
COPY ./ssl /ssl

RUN echo "gid = ${GUID}" >> /app/uwsgi.ini
RUN echo "uid = ${PUID}" >> /app/uwsgi.ini

RUN chown -R ${PUID}:${GUID} /app

# install SSL certificate of PKM server
RUN mkdir -p /usr/local/share/ca-certificates && chmod 755 /usr/local/share/ca-certificates && cp -f /ssl/pkm_docker.crt /usr/local/share/ca-certificates && chmod 644 /usr/local/share/ca-certificates/pkm_docker.crt && update-ca-certificates
