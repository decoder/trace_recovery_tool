# Decoder Trace Recovery tool

This is the Decoder project tool dedicated to the discovery of traceability links between artifacts (WP4)

It is a a Flask service integrated inside a Docker container. It comes along with a second service dedicated to computing similarities with BERT. Both are integrated using Docker compose.

It is currently a basic service examplifying I/O formats and computing a simple BERT-based similarity between its inputs.

Call `build.sh` to build the Docker container. Then `run_docker.sh` to start it
and finally `test_trace_recovery.sh` to test the service. To test its interaction with the PKM, use `test_trace_recovery_pkm.sh`.


## Trace recovery API

The trace recovery tool, when interacting with the pkm, takes as parameters the information necessary to find in the pkm two text segments. Its return value is only a pair of a string and a code indicating the success of the operation or explaining its failure, completed with the id of the artifact added to the PKM traceability matrix collection. The actual result is an annotation added to the Annotations collection of the pkm.

`src_path` and `tgt_path` are expressed using the [JMESPath](https://jmespath.org/) query language.

### Input:
  * project_id: pkm project to query
  * src_path: pkm path to query to retrieve json data containing the source text
  * src_access: json path allowing to retrieve the source text to analyze in the retrieved data
  * tgt_path: pkm path to query to retrieve json data containing the target text
  * tgt_access: json path allowing to retrieve the target text to analyze in the retrieved data
  * model: the trace recovery model to use. Currently unused.

### Output:
  * similarity: the computed similarity value between the two text segments
  * role: the type of traceability link. Currently unused.
  * artifactId: the ID of the artifact added to the PKM traceability matrix collection

### Side effect:

If successful, an artefact is added to the pkm in the array at the path: `/traceability_matrix/{project_id}`. The artefact content is a JSON structure of the form:

```
{
  "src_path": "<string>",
  "src_access": "<string>",
  "src_text": "<string>",
  "tgt_path": "<string>",
  "tgt_access": "<string>",
  "tgt_text": "<string>",
  "trace":
  {
    "similarity": <float>,
    "role": "<string>"
  }
}
```

## Example

A call with two c functions comments (parameters and result values are in reality encoded to make valid json strings, even when it is not explicitly stated).

The two comments are *"Scalar product of two vectors"* and *"Sum of two vectors"*.

Query:

```
{
  "project_id": "mydb",
  "src_path": "code/c/comments/mydb",
  "src_access": "[?global_kind == 'GFun'] | [?loc.pos_start.pos_path == 'examples/vector2.c'].comments | [0]",
  "tgt_path": "code/c/comments/mydb",
  "tgt_access": "[?global_kind == 'GFun'] | [?loc.pos_start.pos_path == 'examples/vector2.c'].comments | [1]"}
}
```

The result with this call is writen in the pkm at `/traceability_matrix/mydb` Its value is:
```
{
  'src_path': 'code/c/comments/mydb',
  'src_access': '[?global_kind == 'GFun'] | [?loc.pos_start.pos_path == 'examples/vector2.c'].comments | [0]',
  "src_text": "<the source text>",
  'tgt_path': 'code/c/comments/mydb',
  'tgt_access': '[?global_kind == 'GFun'] | [?loc.pos_start.pos_path == 'examples/vector2.c'].comments | [1]',
  "tgt_text": "<the target text>",
  'trace': {
    'similarity': 0.19866098555802622,
    'role': '',
    'artifactId': 'xxx'
  }
}
```

### OpenAPI api

The OpenAPI api of this service can be retrieved by pointing a navigator to the
URL of the service. As of 12/19/2020, it is:

```json
{
  "components": {
    "schemas": {
      "TraceRecoveryResult": {
        "properties": {
          "artifactId": {
            "type": "string"
          },
          "role": {
            "type": "string"
          },
          "similarity": {
            "type": "number"
          }
        },
        "required": [
          "artifactId",
          "role",
          "similarity"
        ],
        "type": "object"
      }
    }
  },
  "info": {
    "description": "This is the Decoder project tool dedicated to the discovery of traceability links between artifacts",
    "title": "Decoder Trace Recovery tool",
    "version": "0.0.2"
  },
  "openapi": "3.0.2",
  "paths": {
    "/pkm_trace_recovery": {
      "post": {
        "parameters": [
          {
            "in": "data",
            "name": "src_path",
            "required": true,
            "schema": {
              "type": "string"
            }
          },
          {
            "in": "data",
            "name": "tgt_path",
            "required": true,
            "schema": {
              "type": "string"
            }
          },
          {
            "in": "data",
            "name": "model",
            "required": false,
            "schema": {
              "type": "string"
            }
          },
          {
            "in": "data",
            "name": "project_id",
            "required": true,
            "schema": {
              "type": "string"
            }
          },
          {
            "in": "data",
            "name": "src_access",
            "required": false,
            "schema": {
              "type": "string"
            }
          },
          {
            "in": "data",
            "name": "tgt_access",
            "required": false,
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/TraceRecoveryResult"
                }
              }
            },
            "description": "Trace recovery completed."
          },
          "500": {
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/TraceRecoveryResult"
                }
              }
            },
            "description": "An unexpected error has occurred."
          }
        }
      }
    },
    "/trace_recovery": {
      "post": {
        "parameters": [
          {
            "in": "data",
            "name": "text1",
            "required": true,
            "schema": {
              "type": "string"
            }
          },
          {
            "in": "data",
            "name": "text2",
            "required": true,
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "schema": {
              "$ref": "#/components/schemas/TraceRecoveryResult"
            }
          }
        }
      }
    }
  }
}
```

