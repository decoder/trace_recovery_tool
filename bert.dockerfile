FROM tensorflow/tensorflow:1.15.5-py3

ARG PUID=2031
ARG GUID=2031
ARG BERT_PORT=5555
ARG BERT_PORT_OUT=5556
ARG BERT_HTTP_PORT=8125

ENV PUID ${PUID}
ENV GUID ${GUID}
ENV BERT_PORT ${BERT_PORT}
ENV BERT_PORT_OUT ${BERT_PORT_OUT}
ENV BERT_HTTP_PORT ${BERT_HTTP_PORT}

# CREATE APP USER
RUN groupadd -g ${GUID} user && \
    useradd -r -u ${PUID} -g user user

# --------------------
# Package installation
# --------------------

RUN apt-get clean && apt-get update && apt-get install -y -qq \
        wget \
        unzip

RUN pip install bert-serving-server bert-serving-client flask flask_compress flask_cors flask_json


RUN mkdir -p /BERT
WORKDIR /BERT
RUN wget https://storage.googleapis.com/bert_models/2020_02_20/uncased_L-2_H-128_A-2.zip
RUN unzip uncased_L-2_H-128_A-2.zip

RUN mkdir -p /app
COPY bert-entrypoint.sh /app
RUN chown -R ${PUID}:${GUID} /app
RUN chown -R ${PUID}:${GUID} /BERT

USER user

WORKDIR /app
ENTRYPOINT ["/app/bert-entrypoint.sh"]
CMD []
HEALTHCHECK --timeout=5s CMD curl -f http://localhost:${BERT_HTTP_PORT}/status/server || exit 1
