#!/bin/bash

host="localhost"
port=703
text1="whichptr indicates which xbuffer holds the final iMCU row."
# text2="test the xbuffer holding the final iMCU row."
text2="no correspondance"

command="curl --request POST  --url http://$host:$port/trace_recovery --header 'content-type: application/json'   --data '{\"text1\": \"$text1\",
\"text2\": \"$text2\" }'"

cat <<EOF
command:
$command

response:
$(eval ${command})
EOF
